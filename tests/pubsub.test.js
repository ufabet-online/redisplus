const pubsub = require('../pubsub')


test('pubsub', async () => {
    try{
        let data = ''
        await pubsub.sub({
            a: (d) => { data += 'a' + (d?.v || '-'); },
            b: (d) => { data += 'b' + (d?.v || '-'); },
            c: (d) => { data += 'c' + (d?.v || '-'); },
            d: (d) => { data += 'd' + (d?.v || '-'); },
        }) 



        await pubsub.pub('a', { v: 1 })
        await new Promise(r => setTimeout(r, 100))
        await pubsub.pub('b', { v: 2 })
        await new Promise(r => setTimeout(r, 100))
        await pubsub.pub('c', { v: 3 })
        await new Promise(r => setTimeout(r, 100))
        await pubsub.pub('d', { v: 4 })
        await new Promise(r => setTimeout(r, 200))
        expect(data).toBe('a1b2c3d4')
    }catch(err) {
        await expect(err).toBe('null');
    }
})

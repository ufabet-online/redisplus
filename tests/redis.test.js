const redis = require('../redis')

// client
// cache
// newClient
// get_json
// set_json
// redlock
// once

test('newClient', async () => {
    try{
        const c = redis.newClient()
        await c.set('test', 'x')

        expect(await c.get('test')).toBe('x')
    }catch(err) {
        await expect(err).toBe('null');
    }
})


test('client', async () => {
    try{
        const c = redis.client()
        await c.set('test', 'x', 'EX', 2)

        expect(await c.get('test')).toBe('x')

        await new Promise(r => setTimeout(r, 2001))
        expect(await c.get('test')).toBe(null)
    }catch(err) {
        await expect(err).toBe('null');
    }
})


test('cache', async () => {
    
    const makeData = async () => {
        return { num: '12345678' }
    }

    try{
        let usedCache = ``;
        const data1 = await redis.cache(`TEST`, 1, makeData, () => { usedCache += 'A'})
        expect(data1.num).toBe('12345678')
        expect(usedCache).toBe('')
        
        await new Promise(r => setTimeout(r, 500))
        const data2 = await redis.cache(`TEST`, 1, makeData, () => { usedCache += 'B'})
        expect(data2.num).toBe('12345678')
        expect(usedCache).toBe('B')

        await new Promise(r => setTimeout(r, 600))
        const data3 = await redis.cache(`TEST`, 1, makeData, () => { usedCache += 'C'})
        expect(data3.num).toBe('12345678')
        expect(usedCache).toBe('B')
    }catch(err) {
        console.log(err) 
        expect(err).toBe('null');
    }
})




test('get_json', async () => {
    try{
        await redis.set_json(`TESTJSON`, { j: 1 } , 2)
        const found = await redis.get_json(`TESTJSON`)
        expect(found.j).toBe(1)
        await new Promise(r => setTimeout(r, 2000))
        const expired = await redis.get_json(`TESTJSON`)
        expect(expired).toBe(null)
    }catch(err) {
        console.log(err) 
        expect(err).toBe('null');
    }
})

test('once', async () => {
    let data = `@`;
    const makeData = (key) => async () => {
        await new Promise(r => setTimeout(r, 500))
        data += key.toUpperCase()
    }

    const onBusy = (key) => () => {
        data += key.toLowerCase()
    }

    redis.once(`DOIT_ONCE`, 1000, makeData('A'), onBusy('A'))
    setTimeout( () => {
        redis.once(`DOIT_ONCE`, 1000, makeData('B'), onBusy('B'))    
    }, 100)
    
    setTimeout( () => {
        redis.once(`DOIT_ONCE`, 1000, makeData('C'), onBusy('C'))    
    }, 200)
    
    setTimeout( () => {
        redis.once(`DOIT_ONCE`, 1000, makeData('D'), onBusy('D'))    
    }, 300)
    
    setTimeout( () => {
        redis.once(`DOIT_ONCE`, 1000, makeData('E'), onBusy('E'))    
    }, 400)
    

    setTimeout( () => {
        redis.once(`DOIT_ONCE`, 1000, makeData('F'), onBusy('F'))
    }, 1100)
    

    await new Promise(r => setTimeout(r, 2000))
    expect(data).toBe(`@bcdeAF`)
    try{
        
    }catch(err) {
        console.log(err) 
        expect(err).toBe('null');
    }
})



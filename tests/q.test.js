const q = require('../q')


test('q', async () => {
    try{
        let summary1 = ''
        let summary2 = ''

        q.getQueue('TEST').process(({ data }, done) => {
            if (data.error) {
                summary1 += `X`;
                setTimeout(() => { done(new Error(`Ouch!`)) }, 10)
                return;
            }

            summary1 += `${data.doMe}`;
            done()
        })

        q.getQueue('TEST2').process(({ data }, done) => {
            if (data.error) {
                summary2 += `X`;
                setTimeout(() => { done(new Error(`Ouch!`)) }, 10)
                return;
            }
            summary2 += `${data.doMe}`;
            done()
        })


        const adder1 = q.getQueue('TEST')
        const adder2 = q.getQueue('TEST2')
        
        await adder1.add({ doMe: 1 })
        await adder2.add({ doMe: 'a' })
        await new Promise(r => setTimeout(r, 500))
        
        await adder1.add({ doMe: 2 })
        await adder2.add({ doMe: 'b' })
        await new Promise(r => setTimeout(r, 500))

        await adder1.add({ error: true })
        await adder2.add({ error: true })
        await new Promise(r => setTimeout(r, 500))

        await adder1.add({ doMe: 4 })
        await adder2.add({ doMe: 'd' })
        await new Promise(r => setTimeout(r, 500))

        await adder1.add({ doMe: 5 })
        await adder2.add({ doMe: 'e' })
        await new Promise(r => setTimeout(r, 500))


        expect(summary1).toBe('12X45')
        expect(summary2).toBe('abXde')

    }catch(err) {
        expect(err).toBe('null');
    }
})

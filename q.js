const Queue = require("bull");

const getQueue = (qname) => {
  return new Queue(qname, process.env.REDIS_URL)
}

module.exports = {
  getQueue,
}

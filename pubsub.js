const { newClient } = require("./redis");
const debug = require('debug')('redisplus')

let __PUBLISHER = null;
const pub = async (key, data) => {
  if (!__PUBLISHER) {
    debug("Create publisher...")
    __PUBLISHER = await newClient()
  } else {
    debug("Recycling publisher")
  }

  debug(`publishing ${JSON.stringify(data)}`)
  await __PUBLISHER.publish(key, JSON.stringify(data));
}

const sub = async (listeners) => {
  debug("Create subscriber...")
  const subscriber = newClient();

  const keys = Object.keys(listeners)

  subscriber.on('message', (key, data) => {
    listeners[key] && listeners[key](JSON.parse(data))
  });
  await subscriber.subscribe(keys)


  return () => {
    debug("UNSUBSCRIBE")
    for(let i = 0;i < listeners.length; i ++) {
      const key = keys[i]
      subscriber.unsubscribe(key);
    };
  }
}

module.exports = {
  pub,
  sub,
}

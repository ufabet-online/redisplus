const Redis = require("ioredis");
const Redlock = require('redlock');
const debug = require('debug')('redisplus')

let defaultClient;

debug(`debuging redisplus`)

const client = () => {
  if (!defaultClient) {
    debug(`Getting default client, non exists, creating.`)
    defaultClient =  newClient()
  } else {
    debug(`Getting default client, found, reusing it.`)
  }

  return defaultClient;
}

const newClient = () => {
  debug(`Connecting to ${process.env.REDIS_URL}.`)
  const client = new Redis(process.env.REDIS_URL);
  return client
}

const cache = async (key, ageSeconds, getDataAsync, onUseCache) => {

  const c = client()

  const data = await c.get(key)
  if (!data) {
    const newData = await getDataAsync()
    await c.set(key, JSON.stringify(newData), "EX", ageSeconds )

    return newData;
  } else {
    const d = JSON.parse(data)
    onUseCache && onUseCache(d)

    return d;
  }
}

let __REDLOCK
const redlock = async () => {
  if (!__REDLOCK) {
    __REDLOCK = new Redlock([client()], {
      driftFactor: 0.01,
      retryCount: 10,
      retryDelay: 600,
      retryJitter: 400,
    });
    
    __REDLOCK.on('clientError', function(err) {
      console.error('A redis error has occurred:', err);
    });

  }

  return __REDLOCK;
}

const get_json = async (key) => {
  const c = client()

  const data = await c.get(key)
  if (data) {
    return JSON.parse(data)
  }

  return null;
}

const set_json = async (key, data, ageSeconds) => {
  const c = client()
  await c.set(key, JSON.stringify(data || null), "EX",  ageSeconds)
}


let __LOCKER__  
const onceLocker = () => {
  if (!__LOCKER__) {
    const c = client()
    // debug(`OnceLocker`, typeof c.get, typeof c.evalsha, typeof c.eval,c)

    __LOCKER__  = new Redlock(
      [c],
      {
          driftFactor: 0.01,
          retryCount:  0,
          retryDelay:  200,
          retryJitter:  50,
      }
    );
    
  }

  return __LOCKER__
}

const once = async (key, timeSpan, func, onBusy) => {
  const locker = onceLocker()
  let lock
  try{
    lock = await locker.lock(`LOCK-${key}`, 2000)
  } catch(err) {
    debug(`${key} locked`)
    onBusy && onBusy()
    return;
  }
  
  try{
    const done = await get_json(`ONCEz-${key}`)
    
    if (done) {
      debug(`${key} already done, skipped`)
      onBusy && onBusy()
      return;
    }

    await set_json(`ONCEz-${key}`, { done: true, key }, Math.ceil(timeSpan / 1000))
    await func()
  }catch(err) {
    console.error(`ERROR ${key}`, err)
  }finally {
    lock.unlock().catch(() => {})
  }
}


module.exports = {
  client,
  cache,
  newClient,
  redlock,
  get_json,
  set_json,
  once,
}

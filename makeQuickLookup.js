const redis = require('./redis')
const debug = require('debug')('redisplus')

const qGet = (name, idField, reload) => {
    return async (id) => {
        const redisClient = redis.client()
        const data = await redisClient.get(`quickLookup_${name}_${id}`)
        if (data) {
            debug(`parsing ${name}.${idField}: ${data}`)
            return JSON.parse(data);
        }
        const data2 =  await reload()
        return data2.filter(l => l[idField] === id).pop()
    }
}

const qGetAll = (name, reload) => {
    return  async () => {
        const redisClient = redis.client()
        const data = await redisClient.get(`quickLookup_${name}`)
        if (data) {
            debug(`parsing ${name}.all: ${data}`)
            return JSON.parse(data);
        }
        return await reload()
    }
}

const qReload = (name, idField, asyncGetAll, isActive, cacheAgeSeconds) => {
    return async () => {
        const ls = await asyncGetAll()
        const all = []
        const redisClient = redis.client()
        await Promise.all(ls.map(async l => {
            if (isActive(l)) {
                await redisClient.set(`quickLookup_${name}_${l[idField]}`, JSON.stringify(l), 'EX', cacheAgeSeconds) 
                all.push({ ...l })
            } else {
                await redisClient.del(`quickLookup_${name}_${l[idField]}`) 
            }
        }))

        await redisClient.set(`quickLookup_${name}`, JSON.stringify(all), 'EX', cacheAgeSeconds) 

        return ls
    }
}

const makeQuickLookup = (name, idField, asyncGetAll, isActive, cacheAgeSeconds) => {
    const reload = qReload(name, idField, asyncGetAll, isActive, cacheAgeSeconds)
    const get = qGet(name, idField, reload)
    const getAll = qGetAll(name, reload)
    return {
        reload,
        get,
        getAll,
    }
}
 
module.exports = makeQuickLookup
